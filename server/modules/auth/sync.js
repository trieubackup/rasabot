const router = require('express').Router()

router.get('/sync', async(req, res, next) => {
  res.json({ username: req.user })
})

module.exports = router
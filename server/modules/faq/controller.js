const FAQ = require('../../models/faq')
const Constant = require('../../models/constant')

const Helper = require('../../helper')

exports.all = async (req, res) => {
    const faq = await FAQ.find({}).sort({order: -1})
    res.send(faq)
}

exports.getAnswer = async (req, res) => {
    const { key } = req.params
    const faq = await FAQ.findOne({ key })
    if (faq) return res.send(faq)
    return res.send({})
}

exports.getAnswerRasa = async(req, res) => {
    const { key } = req.query
    const faq = await FAQ.findOne({ key })
    if (faq) return res.send({ content: faq.content })
    return res.send({content: []})
}

exports.getAnswerFB = async(req, res) => {
    const { rawContent } = req.query
    return await Helper.convertContent(rawContent)
}

exports.updateAnswer = async (req, res) => {
    const { id } = req.params
    const payload = req.body
    
    await Helper.convertAnswer(payload)

    await FAQ.findByIdAndUpdate(id, {
        ...payload
    })

    const rs = await FAQ.findOne({key: payload.key}).lean()
    res.send({success: true, ...rs})
}

exports.convertAllContent = async(req, res) => {
    const faqs = await FAQ.find()

    faqs.map(async (faq) => {
        await Helper.convertAnswer(faq)
        await FAQ.updateOne({key: faq.key}, faq)
    })

    res.send(faqs)
}
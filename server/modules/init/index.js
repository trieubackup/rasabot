const router = require('express').Router()
const faqs = require('./data/faq')

const FAQ = require('../../models/faq')

router.get('/db', async(req, res, next) => {
  await FAQ.insertMany(faqs.faqs)
  res.json({ username: req.user })
})

module.exports = router
const FAQ = require('../../models/faq')
const Major = require('../../models/major')

exports.getAnswer = async (req, res) => {
  const { key } = req.params
  const faq = await FAQ.findOne({ key })
  if (faq) {
    return res.json({ content: faq.content })
  }
  return res.send({ content: ['Chưa có câu trả lời cho câu hỏi này'] })
}

exports.getMajor = async (req, res) => {
  const { key } = req.params
  const major = await Major.findOne({ key })
  if (major) {
    return res.json(major)
  }
  return res.send({})
}
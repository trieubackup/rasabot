const Major = require('../../models/major')

exports.getMajors = async (req, res) => {
    const majors = await Major.find({})
    res.send(majors)
}

exports.getMajor = async (req, res) => {
    const { majorName } = req.query
    const major = await Major.findOne({ majorName })
    res.send(major)
}
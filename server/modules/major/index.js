const express = require('express')

const router = express.Router()

const Controller = require('./controller')

router.get('/', Controller.getMajors)
router.get('/:key', Controller.getMajor)
// router.get('/rasa/get-majors')
// router.get('/rasa/get-major')

module.exports = router
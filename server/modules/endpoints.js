const express = require('express')
const router = express.Router()


const authMiddleware = require('../middlewares/auth')
const init = require('./init')
const rasa = require('./rasa')
const auth = require('./auth')
const constant = require('./constant')
const faq = require('./faq')
const major = require('./major')
const sync = require('./auth/sync')

// Rasa
router.use('/rasa', rasa)

// CMS
router.use('/api/init', init)
router.use('/api/auth', auth)
router.use('/api', authMiddleware)
router.use('/api/auth', sync)
router.use('/api/constant', constant)
router.use('/api/faq', faq)
router.use('/api/major', major)

module.exports = router
const constantModel = require('../models/constant')
const Constant = require('../models/constant')
const Major = require('../models/major')

exports.convertContent = async (content) => {
    const constants = await Constant.find()

    return content.map(item => {
        constants.map(ct => {
            item = item.split(`{{${ct.key}}}`).join(ct.value)
        })
        return item
    })
}

exports.convertAnswer = async (answer) => {
    if (!answer.rawContent) return

    const constants = await Constant.find()
    const majors = await Major.find({})

    const content = []
    const htmlContent = []
    answer.rawContent.map((item) => {
        let text = item
        let htmlText = item
        constants.map(ct => {
            text = text.split(`{{${ct.key}}}`).join(ct.value)
            htmlText = htmlText.split(`{{${ct.key}}}`).join(`<span class="mess-constant"><span class="mess-constant__key">{{${ct.key}}}</span><span class="mess-constant__value">${ct.value}</span></span>`)
        })

        if(answer.key === 'ask_all_major') {
            const majorText = majors.map((major) => {
                return `- ${major.majorName} (chỉ tiêu: ${major.target} sinh viên})`    
            }).join('\n')
            text = text.split(`{{all_major}}`).join(majorText)
            htmlText = htmlText.split(`{{all_major}}`).join(`<span class="mess-constant"><span class="mess-constant__key">{{all_major}}</span><span class="mess-constant__value">${majorText}</span></span>`)
        }
    
        content.push(text)
        htmlContent.push(htmlText)
    })

    Object.assign(answer, {content, htmlContent})
}
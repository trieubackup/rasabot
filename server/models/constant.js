const mongoose = require('mongoose')
const Schema = mongoose.Schema

const ConstantSchema = new Schema({
  key: { type: String, required: true, unique: true },
  name: { type: String, required: true },
  value: { type: String, required: true }
}, {
  collection: 'constants'
})

module.exports = mongoose.model('constants', ConstantSchema)
from typing import Any, Text, Dict, List

from rasa_sdk import Action, Tracker
from rasa_sdk.executor import CollectingDispatcher


def getAnswer(key):
  faq = db.faq
  return faq.find_one({"key": key})


def getAllMajor():
  return db.majors.find()


def getMajor(majorName):
  majors = db.majors

  query = {
    "majorName": {
      "$regex": majorName,
      "$options": 'i'
    }
  }
  _major = majors.find_one(query)
  return _major


class ActionMajor(Action):
  def name(self) -> Text:
    return "action_major"

  def run(self, dispatcher: CollectingDispatcher,
      tracker: Tracker,
      domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:

    major = tracker.latest_message['entities']
    print(major)
    res = ""

    if (not major):
      dispatcher.utter_message(text=res)
    else:
      _major = getMajor(major[0]['value'])
      if(_major):
        # res = "Trường đại học Nha Trang hiện đang đào tạo ngành [" + major[0]['value'] + "]"
        # res = "Mã ngành: [" + major[0]['value'] + "]"
        # res = "Chỉ tiêu [" + major[0]['value'] + "]"
        # res = "Tổ hợp xét tuyển [" + major[0]['value'] + "]"
        dispatcher.utter_message(
          text="Trường đại học Nha Trang hiện đang đào tạo ngành [{}]".format(major[0]['value']))
        dispatcher.utter_message(
          text="Mã ngành: {}".format(_major['majorId']))
        dispatcher.utter_message(
          text="Chỉ tiêu: {} thí sinh".format(_major['target']))
        dispatcher.utter_message(text="Tổ hợp xét tuyển: [{}]".format(
          ', '.join(map(str, _major['combination']))))

    return []

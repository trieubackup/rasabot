from typing import Any, Text, Dict, List

from rasa_sdk import Action, Tracker
from rasa_sdk.executor import CollectingDispatcher

import requests

def getAnswer(key):
    res = requests.get('http://cms-server:3030/rasa/get-answer/{}'.format(key))
    return res.json()

class ActionGetAnswer(Action):
    def name(self) -> Text:
        return "action_get_answer"

    def run(self, dispatcher: CollectingDispatcher,
            tracker: Tracker,
            domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:

        key = tracker.latest_message['intent'].get('name')
        print(key)
        content = getAnswer(key)['content']
        if(content):
            for ct in content:
                dispatcher.utter_message(ct)
            
        return []
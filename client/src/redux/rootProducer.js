import { combineReducers } from 'redux'

import userReducer from './User/user.reducer'
import messengerReducer from './Messenger/messenger.reducer'

const rootReducer = combineReducers({
  user: userReducer,
  messenger: messengerReducer
})

export default rootReducer
import {
  SETMESS,
  SETLOADING
} from './messenger.types'

export const setMessenger = (mess) => {
  return {
    type: SETMESS,
    payload: {
      mess
    }
  }
}
export const setLoading = (value) => {
  return {
    type: SETLOADING,
    payload: {
      value
    }
  }
}
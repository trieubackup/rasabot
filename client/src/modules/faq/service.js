import Axios from 'axios'
const apiUrl = 'http://localhost:3000'

const getFAQ = () => Axios.get(`${apiUrl}/get-answers`)
const getAnswer = async (params) => {
    return Axios.get(`${apiUrl}/get-answer`, {
        params
    })
}
const getFbAnswer = async (params) => {
    return Axios.get(`${apiUrl}/get-fb-answer`, {
        params
    })
}
const updateAnswer = (payload) => Axios.post(`${apiUrl}/update-answer`, {
    ...payload
})
export default {
    getFAQ,
    getAnswer,
    getFbAnswer,
    updateAnswer
}
import React, { useState, useEffect, useContext } from 'react'
import { connect } from 'react-redux'
import {
  setMessenger,
  setLoading,
} from '../../redux/Messenger/messenger.actions'
import {
  useHistory
} from "react-router-dom";
import './style.css'

import API from '../../api'

const FAQ = (props) => {
  const [faq, setFaq] = useState([])
  const [selected, setSelected] = useState('')
  const history = useHistory()

  const { setMessenger, setLoading } = props

  useEffect(() => {
    const fetchData = async () => {
      const { data } = await API.getAnswers()
      setFaq(data)
    }

    fetchData()
  }, [])

  const onRowClick = (e, question) => {
    switch(e.detail) {
      case 1:
        setSelected(question.key)
        setMessenger({
          key: question.key,
          content: question.content,
          htmlContent: question.htmlContent,
        })
        break
      case 2:
        history.push(`/faq/${question.key}`)
        break

      default:
    }
  }
      
  const Table = () => {
    return (
      <div className="table bg-white flex flex-col flex-1">
        <div className="table-header">
          <div className="table-row flex">
            <div className="width-50 font-bold uppercase">Mã</div>
            <div className="width-50 font-bold uppercase">Mô tả</div>
          </div>
        </div>
        <div className="table-body">
        { faq.map(question => (
          <div
            className={`table-row flex pointer ${selected === question.key ? 'selected' : ''}`}
            key={question.key}
            onClick={(e) => onRowClick(e, question)}
          >
            <div className="width-50">{question.key}</div>
            <div className="width-50">
              {question.description}
            </div>
          </div>
        ))}
        </div>
      </div>
    )
  }

  return (
    <div className="faq flex flex-1 flex-col">
      <h1 className="uppercase">Câu hỏi</h1>
      <div className="flex flex-1 mt-16">
        { faq.length ? <Table /> : <h1>Loading...</h1>}
      </div>
    </div>
  )
}
const mapStateToProps = (state) => {
  return {}
}
const mapDispathToProps = (dispatch) => {
  return {
    setMessenger: (mess) => dispatch(setMessenger(mess)),
    setLoading: (value = true) => dispatch(setLoading(value))
  }
}
export default connect(mapStateToProps, mapDispathToProps)(FAQ)
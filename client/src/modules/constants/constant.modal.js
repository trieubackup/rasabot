import React, {useRef, useEffect} from 'react'

const Modal = ({formData, onChange, onSubmit, onClose, onDelete }) => {
  const { _id, key, name, value } = formData

  const modal = useRef(null)
  const modalContent = useRef(null)

  useEffect(() => {
    const parent = modal.current
    const child = modalContent.current

    const clickOutsideHandle = (e) => {
      if(e.target !== child && !child.contains(e.target)) {
        onClose()
        return
      }
    }

    parent.addEventListener('click', clickOutsideHandle)
    return () => {
      parent.removeEventListener('click', clickOutsideHandle)
    }
  }, [onClose])

  return (
    <div className="modal" ref={modal}>
      <div className="modal-content border" ref={modalContent}>
        <div className="flex justify-content-between align-items-center">
          <h2>{ _id ? "Chỉnh sửa một biến" : "Thêm một biến" }</h2>
          <button className="btn bg-primary text-white uppercase" onClick={(e) => onDelete()}>Xoá</button>
        </div>
        <hr className="mt-8 mb-32"/>
        <form className="form">
          <input
            type="hidden"
            name="_id"
            value={_id}
          />
          <div className="form-input">
            <label>Mã</label>
            <input
              type="text"
              name="key"
              value={key}
              onChange={(e) => onChange(e)}
            />
          </div>
          
          <div className="form-input">
            <label>Tên</label>
            <input
              type="text"
              name="name"
              value={name}
              onChange={(e) => onChange(e)}
            />
          </div>

          <div className="form-input">
            <label>Giá trị</label>
            <textarea
              type="text"
              name="value"
              value={value}
              spellCheck="false"
              rows="5"
              onChange={(e) => onChange(e)}
            />
          </div>
        </form>
        <div className="flex align-item-center justify-content-between">
          <button className="btn" onClick={(e) => { onSubmit() }}>Huỷ</button>
          <button className="btn" onClick={(e) => { onSubmit() }}>Lưu</button>
        </div>
      </div>
    </div>
  )
}

export default Modal
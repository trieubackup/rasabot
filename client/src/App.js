import React, { useEffect } from 'react'
import {
  BrowserRouter as Router,
  Switch,
  Route,
} from "react-router-dom";

import API from './api'
import { setUser } from './redux/User/user.actions'
import { connect } from 'react-redux'

// Helper
import { getAccessToken } from './helpers'

// Component
import Header from './components/header'
import Messenger from './components/messenger'
import Major from './modules/major'
import FAQ from './modules/faq'
import FaqEditor from './modules/faq/faq-editor'
import Constant from './modules/constants'
import Login from './modules/auth/login'

const App = (props) => {
  const { user, setUser } = props

  useEffect(() => {
    const sync = async (token) => {
      const data = await API.sync(token)
      setUser(data)
    }

    const token = getAccessToken()
    if (token) {
      sync(token)
    }
  }, [])

  const MainContent = () => (
    <>
      <div className="main-content-wrapper bg-secondary">
        <div className="main-content">
          <Switch>
            <Route path="/faq/:key">
              <FaqEditor />
            </Route>
            <Route path="/major">
              <Major />
            </Route>
            <Route path="/faq">
              <FAQ />
            </Route>
            <Route path="/constant">
              <Constant />
            </Route>
            <Route path="/">
              <h1>Home</h1>
            </Route>
          </Switch>
        </div>
        <div className="messenger-container d-flex">
          <Messenger />
        </div>
      </div>
    </>
  )

  return (
    <Router>
      <div className="App flex">
        <Header />
        {!user.isActive ? <Login /> : <MainContent />}
      </div>
    </Router>
  );
}

const mapStateToProps = (state) => {
  return {
    user: state.user
  }
}
const mapDispatchToProps = (dispatch) => {
  return {
    setUser: (user) => dispatch(setUser(user))
  }
}
export default connect(mapStateToProps, mapDispatchToProps)(App);

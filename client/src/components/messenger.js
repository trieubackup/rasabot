import React from 'react'
import { connect } from 'react-redux'

const Messenger = (props) => {
  const { fkey, htmlContent, isLoading } = props

  const Content =  () => (
    <>
      <p className="italic">{fkey}</p>
      { htmlContent && htmlContent.map((item, index) => (
        <p dangerouslySetInnerHTML={{__html: item}} key={index} className="mess-item" />
      )) }
    </>
  )

  const Loading = () => (
    <div className="loading">
      I'm loading
    </div>
  )

  return (
    <div className="messenger bg-white border flex flex-col ml-16">
      <div className="border-bottom-3 flex p-8">
        <div className="mess-avt mr-8 circle size-32 bg-gray relative"/>
        <div className="flex flex-col">
          <span className="uppercase font-bold">NTU Chatbot</span>
          <small className="italic">Activate now</small>
        </div>
      </div>
      <div className="mess-body overlay h-100 p-16">
      { isLoading ? <Loading/> : <Content/> }
      </div>
    </div>
  )
}

const mapStateToProps = state => {
  return {
    fkey: state.messenger.key,
    content: state.messenger.content,
    htmlContent: state.messenger.htmlContent,
    isLoading: state.messenger.isLoading
  }
}

export default connect(mapStateToProps)(Messenger)
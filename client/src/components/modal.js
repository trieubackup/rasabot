import React, {useRef, useEffect} from 'react'

const Modal = ({children, onClickOutside, onClickSubmit, onClickClose}) => {
    const modal = useRef(null)
    const modalContent = useRef(null)

    useEffect(() => {
        const parent = modal.current
        const child = modalContent.current

        const clickOutsideHandle = (e) => {
            if(e.target !== child && !child.contains(e.target)) {
                onClickOutside()
                return
            }
        }

        parent.addEventListener('click', clickOutsideHandle)
        return () => {
            parent.removeEventListener('click', clickOutsideHandle)
        }
    }, [])

    return (
        <div className="modal" ref={modal}>
            <div className="modal-content" ref={modalContent}>
                { children }
                <div className="">
                    <button onClick={(e) => { onClickSubmit() }}>Lưu</button>
                </div>
            </div>
        </div>
    )
}

export default Modal
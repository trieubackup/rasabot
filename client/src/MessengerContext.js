import React from 'react'

const MessengerContext = React.createContext(null)

export default MessengerContext